const int ledPin = 13;

int main() {
    // initialize the digital pin as an output.
    pinMode(ledPin, OUTPUT);

    for (;;) {
        digitalWrite(ledPin, HIGH);   // set the LED on
        delay(100);                  // wait for a second
        digitalWrite(ledPin, LOW);    // set the LED off
        delay(1000);                  // wait for a second
    }

    return 0;
}