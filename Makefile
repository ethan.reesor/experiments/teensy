MAKEFILE := $(abspath $(lastword $(MAKEFILE_LIST)))
DIR := $(dir $(MAKEFILE))

ARDUINO := /Applications/Arduino.app/Contents/Java
HW := $(ARDUINO)/hardware
LIBS := $(ARDUINO)/libraries

FQBN := teensy:avr:teensy36:usb=serial,speed=180,opt=o2std,keys=en-us
PORT := $(shell $(HW)/tools/teensy_ports -L | grep 'Teensy 3.6' | awk '{ print $$1 }')

AFLAGS := -hardware $(HW) -tools $(ARDUINO)/tools-builder -built-in-libraries $(LIBS) -fqbn=$(FQBN)
TFLAGS := -path=$(DIR)build -tools=$(HW)/tools -board=TEENSY36

.PHONY: load restart clean all discover

all: build/main.c.elf

clean:
	rm -rf $(DIR)build $(DIR)cache

build:
	mkdir -p $(DIR)build

cache:
	mkdir -p $(DIR)cache

discover:
	@echo $(PORT)

build/main.c.elf: build cache main.c
	$(ARDUINO)/arduino-builder -compile $(AFLAGS) -build-path $(DIR)build -build-cache $(DIR)cache $(DIR)main.c

load: all
	$(HW)/tools/teensy_post_compile $(TFLAGS) -file=main.c -reboot -port=$(PORT) -portprotocol=serial